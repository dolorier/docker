FROM elifarley/docker-oracle-jdk6 

MAINTAINER Avantica Technologies

ENV JAVA_HOME /usr/lib/jvm/java-6-oracle
ENV MW_HOME /opt/weblogic
ENV PATH $PATH:$MW_HOME:$JAVA_HOME/bin

COPY wls1036_dev.zip /opt
COPY silent.xml /opt
COPY wl.rsp /opt

RUN apt-get update \
	&& apt-get install -y unzip \
	&& cd /opt \
	&& unzip -d weblogic "wls1036_dev.zip" \
	&& rm "wls1036_dev.zip" \
	&& bash /opt/weblogic/configure.sh -mode=silent -silnet_xml=/opt/silent.xml -log=/home/install-wl.txt -log_priority=debug 

EXPOSE 7001
WORKDIR /home
CMD /bin/bash
