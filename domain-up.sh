#!/bin/bash

if [[ ! -d /opt/weblogic/user_projects/domains/entel_domain/ ]]; then
	bash /opt/weblogic/wlserver/common/bin/config.sh -mode=silent -silent_script=/home/wl.rsp -log=install-domain.txt -log_priority=debug
fi
bash /opt/weblogic/user_projects/domains/entel_domain/startWebLogic.sh
